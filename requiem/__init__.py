
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


__author__ = "God Empress Verin"

__contributors__ = ["Zevfer", "Adam", "XLR", "Espy", "Raatty", "Dusty.P", "UniQ", "Lunar", "Puffdip", "Sebi", "Ameyuri",
                    "Hydraik", "Akuryo", "Venom", "RafSun", "Fireborn", "Cobrastrike", "Minesomemc", "Melyaj Vijsopj",
                    "SOL Slayer", "Kiddterra Boki", "George"]

__title__ = "Requiem Project"

__version__ = "0.5.0"

__repository__ = "https://gitlab.com/AnakiKaiver297/Requiem-Project"

__description__ = """Requiem is a multi-purpose tool designed to provide large number of features 
                     in a configurable and easy-to-use package. Features range from moderation to 
                     entertainment to automation. Features include music playback, member greeting 
                     and automatic role assignment, PWNS (Politics and War Notification Service) 
                     as well as many more! A full list of functions and modules can be found by 
                     running the help command "{}help"!"""
