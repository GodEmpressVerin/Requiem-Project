
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from .funcs import build_reactions
import asyncio


class Confirminator:

	def __init__(self, ctx, message, timeout=300):
		self.ctx = ctx
		self.bot = ctx.bot
		self.message = message
		self.timeout = timeout
		self.expired = False
		self.failed = False
		self.current_time = 0
		self.reacts = ['✅', '❎']

	def check(self, reaction, user):
		return self.ctx.author.id == user.id and reaction.emoji in self.reacts

	async def navigate(self):
		self.bot.loop.create_task(build_reactions(self))

		while not self.expired:

			try:
				reaction, user = await self.bot.wait_for("reaction_add", timeout=self.timeout, check=self.check)
				result = await self.handle_reaction(reaction, user)
				return result
			except asyncio.TimeoutError:
				self.current_time = self.timeout
				return False

		if self.failed:
			raise self.failed

	@staticmethod
	async def handle_reaction(reaction, user):
		if reaction.emoji == '✅':
			return True
		elif reaction.emoji == '❎':
			return False
