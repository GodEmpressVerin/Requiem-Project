
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
import asyncio
import aiohttp


async def build_reactions(obj):
    while obj.current_time != obj.timeout:
        try:
            try:
                obj.message = await obj.message.channel.fetch_message(obj.message.id)
                current_reacts = [reaction.emoji for reaction in obj.message.reactions]

                for react in obj.reacts:
                    if react not in current_reacts:
                        await obj.message.add_reaction(react)

            except discord.errors.NotFound as exception:
                obj.current_time = obj.timeout
                obj.failed = exception

            except discord.errors.Forbidden as exception:
                obj.current_time = obj.timeout
                obj.failed = exception

            await asyncio.sleep(1)
        except (aiohttp.ServerDisconnectedError, discord.NotFound):
            return


def line_check(text: str, lines: int):
    pages = []
    output = ""
    for line in text.split("\n"):
        if len(line) > 0:
            if len(output.split("\n")) + 1 > lines:
                pages.append(output)
                output = line
            else:
                output += line + "\n"
    if output not in pages:
        pages.append(output)
    return pages


def length_check(text: str, length: int):
    line = ""
    output = ""
    for word in text.split(" "):
        if len(line) + len(word) > length:
            output += line + "\n"
            line = word + " "
        else:
            line += word + " "
    if line not in output:
        output += line
    return output


async def timeout_handler(obj):
    while obj.current_time != obj.timeout:
        obj.current_time += 1
        await asyncio.sleep(1)
