
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from discord.ext.commands import errors
import contextlib


async def check_cog_run_status(ctx, commands):
    """Checks cog run status to verify functions in it can be executed by the user"""
    can_run = []
    for command in commands:
        with contextlib.suppress(errors.CommandError):
            check = await command.can_run(ctx)
            if check:
                can_run.append(command)
    return can_run


def get_help(command, prefix):
    """Gets the help text for the given command"""
    return command.help.replace('%prefix%', prefix) if command.help else "There is no help text for this command!"


def get_brief(command, prefix):
    """Gets the brief text for the given command"""
    return command.brief.replace('%prefix%', prefix) if command.brief else "No short help text available for this command!"


def get_usage(command, prefix):
    """Gets the usage for a given command"""
    command_title = f"{command.parent.name} {command.name}" if command.parent else command.name
    usage_end = command.usage if command.usage else command.signature
    usage = f"{prefix}{command_title} {usage_end}"
    return usage


async def generate_dicts(ctx):
    """Generates a dict of functions and cogs for use in the help command"""
    cogs_dict = {}
    commands_dict = {}

    for cog in ctx.bot.cogs:
        cog = ctx.bot.get_cog(cog)

        if not str(cog.__doc__).startswith("!"):

            
            cog_name = cog.name if hasattr(cog, 'name') else cog.__cog_name__
            cog_help = str(cog.__doc__) if cog.__doc__ else "There is no help text for this module!"

            cog_commands = {}
            commands = cog.get_commands()
            commands = await check_cog_run_status(ctx, commands)

            for command in commands:
                command = ctx.bot.get_command(f"{command}")
                commands_dict[command.name] = command
                cog_commands[command.name] = command

                if hasattr(command, "walk_commands"):
                    for child in command.walk_commands():
                        commands_dict[child.parent.name + " " + child.name] = child
                        cog_commands[child.parent.name + " " + child.name] = child

            cogs_dict[cog_name.replace("'", "").lower()] = (cog_name, cog_help, cog_commands)

    return cogs_dict, commands_dict
