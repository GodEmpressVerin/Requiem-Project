CREATE TABLE IF NOT EXISTS users(
    user_id         BIGINT              PRIMARY KEY,
    nation_id       INTEGER             DEFAULT 0,
    war_alerts      BOOLEAN             DEFAULT False
)