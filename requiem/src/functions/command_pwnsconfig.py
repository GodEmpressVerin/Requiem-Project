# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import asyncio
import json

import discord
from discord.ext import commands
from pwpy import exceptions, scrape

from core.engine.utils import reaction_factory
from src.functions.utils import clean_up, handle_timeout, runnable

title = "PWNS Configurator - "


async def tgtsdictstring(key, startdict, pnw, ctx):
    startdict = json.loads(startdict)
    enddict = {alliance: values for alliance, values in startdict.items()
               if discord.utils.get(ctx.guild.channels, id=values["channel"])
               if await scrape.lookup(pnw.alliances_latest, id=alliance)}

    tgts = "No Targets Being Monitored" if len(enddict) == 0 else ", ".join(enddict.keys())

    if enddict != startdict:
        async with ctx.bot.pool.acquire() as conn:
            await conn.execute(f"UPDATE guilds SET {key} = ($1) WHERE guild_id = ($2)",
                               f"{enddict}".replace("'", '"'), ctx.guild.id)

    return enddict, tgts


async def aachanstring(ctx, pnw, offtgtsdict):
    channels = []
    for alliance, values in offtgtsdict.items():
        alliance = await scrape.lookup(pnw.alliances_latest, id=alliance)
        channel = discord.utils.get(ctx.guild.channels, id=values['channel'])
        channels.append(f"{alliance.name} {channel.mention}")
    return "\n".join(channels)


async def run_pwnsconfig(ctx):
    pnw = ctx.bot.pw

    if pnw.nations_latest and pnw.alliances_latest:
        starter = check_perms, ctx, pnw
        await runnable(starter)

    else:
        embed = discord.Embed(
            title=title + "PWPY is not ready yet!",
            description="PWPY has not finished caching nations and alliances! Please wait a few minutes and try again!",
            colour=ctx.colour
        )
        await ctx.send(embed=embed)


async def check_perms(ctx, pnw):
    if ctx.guild and ctx.author.guild_permissions.manage_guild:
        embed = discord.Embed(title=title + "Selection Menu", color=ctx.colour)
        embed.add_field(name="📍 Guild Configuration", value="View this guilds PWNS configuration", inline=False)
        embed.add_field(name="🚩 Personal Configuration", value="View your personal PWNS configuration", inline=False)
        message = await ctx.send(embed=embed)

        reacts = ["📍", "🚩", "❎"]
        pages = [guild_menu, personal_menu, None]
        chosen = await reaction_factory.Menunator(ctx, message, pages, reacts).navigate()
        await clean_up(message)
        return chosen, ctx, pnw

    return personal_menu, ctx, pnw


async def personal_menu(ctx, pnw):
    config = ctx.user_config
    nation = await scrape.lookup(pnw.nations_latest, id=config.nation_id)

    natstr = f"**{nation.name}**" if nation else "Your account is not currently linked to a nation! Run the command " \
                                                 f"{ctx.loc_prefix}registerpw to link your discord account to a nation!"
    if nation:
        wmstr = f"You {'currently' if config.war_alerts else 'not currently'} receiving war notifications!"
    else:
        wmstr = "You must link your discord account with a nation before you can configure the war monitor!"

    embed = discord.Embed(title=title + "Personal Menu", colour=ctx.colour)
    embed.add_field(name="Nation", value=natstr, inline=False)
    embed.add_field(name="⚔ War Monitor", value=wmstr, inline=False)
    message = await ctx.send(embed=embed)

    pages = [personal_war, None]
    reacts = ["⚔", "❎"]
    chosen = await reaction_factory.Menunator(ctx, message, pages, reacts).navigate()
    await clean_up(message)
    return chosen, ctx, pnw


async def personal_war(ctx, pnw):
    config = ctx.user_config
    nation = await scrape.lookup(pnw.nations_latest, id=config.nation_id)

    if config.war_alerts:
        current = " "
        setas = False
        after = "disabled"
    else:
        current = " not "
        setas = True
        after = "enabled"

    if nation:
        wmstr = f"You are{current}currently receiving war notifications! " \
                f"Would you like to toggle it?"
    else:
        wmstr = "You must link your discord account with a nation before you can configure the war monitor!"

    embed = discord.Embed(title=title + "Personal - War Monitor", description=wmstr, colour=ctx.colour)
    message = await ctx.send(embed=embed)

    if nation:
        confirm = await reaction_factory.Confirminator(ctx, message).navigate()
        await clean_up(message)

        if confirm:
            embed = discord.Embed(
                title=title + "Personal - War Monitor",
                description=f"Alright! The war monitor is now {after}!",
                colour=ctx.colour)
            message = await ctx.send(embed=embed)
            async with ctx.bot.pool.acquire() as conn:
                await conn.execute("UPDATE users SET war_alerts = ($1) WHERE user_id = ($2)", setas, ctx.author.id)

    await asyncio.sleep(10)
    await clean_up(message)
    return personal_menu, ctx, pnw


async def guild_menu(ctx, pnw):
    embed = discord.Embed(title=title + "Guild Menu", colour=ctx.colour)
    embed.add_field(name="⚔ War Monitor", value="Configure this guilds war monitor", inline=False)
    embed.add_field(name="💼 Applicant Monitor", value="Configure this guilds applicant monitor", inline=False)
    embed.add_field(name="🏳 Beige Monitor", value="Configure this guilds beige monitor", inline=False)
    message = await ctx.send(embed=embed)

    pages = [guild_war, guild_app, guild_beige, None]
    reacts = ["⚔", "💼", "🏳", "❎"]
    chosen = await reaction_factory.Menunator(ctx, message, pages, reacts).navigate()
    await clean_up(message)
    return chosen, ctx, pnw


async def guild_war(ctx, pnw, *args):
    config = ctx.guild_config
    offtgtsdict, offtgts = await tgtsdictstring("offensive_monitor", config.offensive_monitor, pnw, ctx)
    deftgtsdict, deftgts = await tgtsdictstring("defensive_monitor", config.defensive_monitor, pnw, ctx)

    embed = discord.Embed(title=title + "Guild - War Monitor", colour=ctx.colour)
    embed.add_field(name="⚔ Offensive Wars", value=offtgts, inline=False)
    embed.add_field(name="🛡 Defensive Wars", value=deftgts, inline=False)
    message = await ctx.send(embed=embed)

    pages = [guild_offwar, guild_defwar, guild_menu, None]
    reacts = ["⚔", "🛡", "◀", "❎"]
    chosen = await reaction_factory.Menunator(ctx, message, pages, reacts).navigate()
    await clean_up(message)
    return chosen, ctx, pnw


async def guild_beige(ctx, pnw):
    config = ctx.guild_config
    offtgtsdict, offtgts = await tgtsdictstring("beige_monitor", config.beige_monitor, pnw, ctx)
    channels = await aachanstring(ctx, pnw, offtgtsdict)
    embed = discord.Embed(title=title + "Guild - Beige Monitor", description=channels, colour=ctx.colour)
    embed.add_field(name="➕ Add Alliance", value="Add an alliance to be monitored for beige.", inline=False)
    embed.add_field(name="➖ Remove Alliance", value="Remove an alliance from being monitored for beige.",
                    inline=False),
    embed.add_field(name="🆑 Clear Alliances", value="Clear the targets list.", inline=False)
    message = await ctx.send(embed=embed)

    reacts = ["➕", "➖", "🆑", "◀", "❎"]
    pages = [add_alliance, remove_alliance, clear_alliances, guild_war, None]
    chosen = await reaction_factory.Menunator(ctx, message, pages, reacts).navigate()
    await clean_up(message)
    aptitle = title + "Guild - Beige Monitor"
    return chosen, ctx, pnw, aptitle, "beige", "beige_monitor", offtgtsdict, guild_beige


async def guild_app(ctx, pnw):
    config = ctx.guild_config
    offtgtsdict, offtgts = await tgtsdictstring("applicant_monitor", config.applicant_monitor, pnw, ctx)
    channels = await aachanstring(ctx, pnw, offtgtsdict)
    embed = discord.Embed(title=title + "Guild - Applicant Monitor", description=channels, colour=ctx.colour)
    embed.add_field(name="➕ Add Alliance", value="Add an alliance to be monitored for applicants.", inline=False)
    embed.add_field(name="➖ Remove Alliance", value="Remove an alliance from being monitored for applicants.",
                    inline=False),
    embed.add_field(name="🆑 Clear Alliances", value="Clear the targets list.", inline=False)
    message = await ctx.send(embed=embed)

    reacts = ["➕", "➖", "🆑", "◀", "❎"]
    pages = [add_alliance, remove_alliance, clear_alliances, guild_war, None]
    chosen = await reaction_factory.Menunator(ctx, message, pages, reacts).navigate()
    await clean_up(message)
    aptitle = title + "Guild - Applicant Monitor"
    return chosen, ctx, pnw, aptitle, "applicant", "applicant_monitor", offtgtsdict, guild_app


async def guild_offwar(ctx, pnw):
    config = ctx.guild_config
    offtgtsdict, offtgts = await tgtsdictstring("offensive_monitor", config.offensive_monitor, pnw, ctx)
    channels = await aachanstring(ctx, pnw, offtgtsdict)
    embed = discord.Embed(title=title + "Guild - War Monitor - Offensive Wars", description=channels, colour=ctx.colour)
    embed.add_field(name="➕ Add Alliance", value="Add an alliance to be monitored for offensive wars.", inline=False)
    embed.add_field(name="➖ Remove Alliance", value="Remove an alliance from being monitored for offensive wars.",
                    inline=False),
    embed.add_field(name="🆑 Clear Alliances", value="Clear the targets list.", inline=False)
    message = await ctx.send(embed=embed)

    reacts = ["➕", "➖", "🆑", "◀", "❎"]
    pages = [add_alliance, remove_alliance, clear_alliances, guild_war, None]
    chosen = await reaction_factory.Menunator(ctx, message, pages, reacts).navigate()
    await clean_up(message)
    aptitle = title + "Guild - War Monitor - Offensive War Monitor"
    return chosen, ctx, pnw, aptitle, "offensive war", "offensive_monitor", offtgtsdict, guild_offwar


async def guild_defwar(ctx, pnw):
    config = ctx.guild_config
    offtgtsdict, deftgts = await tgtsdictstring("defensive_monitor", config.defensive_monitor, pnw, ctx)
    channels = await aachanstring(ctx, pnw, offtgtsdict)
    embed = discord.Embed(title=title + "Guild - War Monitor - Defensive Wars", description=channels, colour=ctx.colour)
    embed.add_field(name="➕ Add Alliance", value="Add an alliance to be monitored for defensive wars.", inline=False)
    embed.add_field(name="➖ Remove Alliance", value="Remove an alliance from being monitored for defensive wars.",
                    inline=False),
    embed.add_field(name="🆑 Clear Alliances", value="Clear the targets list.", inline=False)
    message = await ctx.send(embed=embed)

    reacts = ["➕", "➖", "🆑", "◀", "❎"]
    pages = [add_alliance, remove_alliance, clear_alliances, guild_war, None]
    chosen = await reaction_factory.Menunator(ctx, message, pages, reacts).navigate()
    await clean_up(message)
    aptitle = title + "Guild - War Monitor - Defensive War Monitor"
    return chosen, ctx, pnw, aptitle, "defensive", "defensive_monitor", offtgtsdict, guild_defwar


async def clear_alliances(ctx, pnw, aptitle, monitor, sqlstr, existing, return_func):
    if len(existing) > 0:
        embed = discord.Embed(
            title=aptitle + " - Clear Alliances",
            description=f"Are you sure you wish to clear all alliances being targeted by the {monitor} monitor? "
                        "This action is not reversible!",
            colour=ctx.colour
        )
        message = await ctx.send(embed=embed)
        confirm = await reaction_factory.Confirminator(ctx, message).navigate()
        await clean_up(message)
        if confirm:
            async with ctx.bot.pool.acquire() as conn:
                await conn.execute(f"UPDATE guilds SET {sqlstr} = ($1) WHERE guild_id = ($2)", "{}", ctx.guild.id)
            await asyncio.sleep(1)
        return return_func, ctx, pnw

    else:
        embed = discord.Embed(
            title=aptitle + " - Clear Alliances",
            description="There are no alliances being monitored!",
            colour=ctx.colour
        )
        message = await ctx.send(embed=embed)
        await asyncio.sleep(10)
        await clean_up(message)
        return return_func, ctx, pnw


async def remove_alliance(ctx, pnw, aptitle, monitor, sqlstr, existing, return_func):
    if len(existing) > 0:
        embed = discord.Embed(
            title=aptitle + " - Remove Alliance",
            description="Alright! At this time, please say the name or id of the alliance you wish to stop targeting "
                        f"with the {monitor} monitor! Otherwise, say 'cancel' to abort the configuration!",
            colour=ctx.colour
        )
        message = await ctx.send(embed=embed)

        try:
            response = await ctx.bot.wait_for("message", timeout=60, check=lambda m: m.author == ctx.author)
            await clean_up(message)
        except asyncio.TimeoutError:
            await clean_up(message)
            embed = discord.Embed(
                title=aptitle + " - Remove Alliance",
                description="You timed out! Would you like to try again?",
                colour=ctx.colour
            )
            message = await ctx.send(embed=embed)
            confirm = await reaction_factory.Confirminator(ctx, message).navigate()
            await clean_up(message)
            if confirm:
                return add_alliance, ctx, pnw, aptitle, monitor, sqlstr, existing, return_func
            else:
                return return_func, ctx, pnw

        if response.content.lower() == "cancel":
            return return_func, ctx, pnw

        alliance = await scrape.lookup(pnw.alliances_latest, name=response.content, id=response.content)

        if alliance:
            target_id = alliance.id
        else:
            target_id = response.content

        if str(target_id) in existing.keys():
            existing.pop(str(target_id))
            embed = discord.Embed(
                title=aptitle + " - Remove Alliance",
                description=f"Alright! The alliance id {target_id} is no longer being targeted by the"
                f" {monitor} monitor!",
                colour=ctx.colour
            )
            message = await ctx.send(embed=embed)
            async with ctx.bot.pool.acquire() as conn:
                await conn.execute(f"UPDATE guilds SET {sqlstr} = ($1) WHERE guild_id = ($2)",
                                   f"{existing}".replace("'", '"'), ctx.guild.id)
            await asyncio.sleep(10)
            await clean_up(message)
            return return_func, ctx, pnw

        else:
            embed = discord.Embed(
                title=aptitle + " - Remove Alliance",
                description="I wasn't able to verify the alliance given! Would you like to try again?",
                colour=ctx.colour
            )
            message = await ctx.send(embed=embed)
            confirm = await reaction_factory.Confirminator(ctx, message).navigate()
            await clean_up(message)
            if confirm:
                return remove_alliance, ctx, pnw, aptitle, monitor, sqlstr, existing, return_func
            else:
                return return_func, ctx, pnw
    else:
        embed = discord.Embed(
            title=aptitle + " - Remove Alliance",
            description="There are no alliances being monitored!",
            colour=ctx.colour
        )
        message = await ctx.send(embed=embed)
        await asyncio.sleep(10)
        await clean_up(message)
        return return_func, ctx, pnw


async def add_alliance(ctx, pnw, aptitle, monitor, sqlstr, existing, return_func):
    if len(existing) < 15:
        embed = discord.Embed(
            title=aptitle + " - Add Alliance",
            description="Alright! At this time, please say the name or id of the alliance you wish to target with the "
                        f"{monitor} monitor! Otherwise, say 'cancel' to abort the configuration!",
            colour=ctx.colour
        )
        message = await ctx.send(embed=embed)

        try:
            response = await ctx.bot.wait_for("message", timeout=60, check=lambda m: m.author == ctx.author)
            await clean_up(message)
        except asyncio.TimeoutError:
            await clean_up(message)
            embed = discord.Embed(
                title=aptitle + " - Add Alliance",
                description="You timed out! Would you like to try again?",
                colour=ctx.colour
            )
            message = await ctx.send(embed=embed)
            confirm = await reaction_factory.Confirminator(ctx, message).navigate()
            await clean_up(message)
            if confirm:
                return add_alliance, ctx, pnw, aptitle, monitor, sqlstr, existing, return_func
            else:
                return return_func

        if response.content.lower() == "cancel":
            return return_func, ctx, pnw

        alliance = await scrape.lookup(pnw.alliances_latest, name=response.content, id=response.content)

        if alliance:
            if str(alliance.id) in existing.keys():
                embed = discord.Embed(
                    title=aptitle + " - Add Alliance",
                    description=f"The alliance **{alliance.name}** is already being targeted by the {monitor} monitor! "
                                "Would you like to try again?",
                    colour=ctx.colour
                )
                message = await ctx.send(embed=embed)
                confirm = await reaction_factory.Confirminator(ctx, message).navigate()
                await clean_up(message)
                if confirm:
                    return add_alliance, ctx, pnw, aptitle, monitor, sqlstr, existing, return_func
                else:
                    return return_func, ctx, pnw

            else:
                embed = discord.Embed(
                    title=aptitle + " - Add Alliance",
                    description=f"Alright! I've found the alliance **{alliance.name}**! Is this correct?",
                    colour=ctx.colour
                )
                message = await ctx.send(embed=embed)
                confirm = await reaction_factory.Confirminator(ctx, message).navigate()
                await clean_up(message)
                if confirm:
                    return set_alliance_channel, ctx, pnw, aptitle, monitor, sqlstr, existing, alliance, return_func
                else:
                    embed = discord.Embed(
                        title=aptitle + " - Add Alliance",
                        description="Sorry about that, did you want to try again?",
                        colour=ctx.colour
                    )
                    message = await ctx.send(embed=embed)
                    confirm = await reaction_factory.Confirminator(ctx, message).navigate()
                    await clean_up(message)
                    if confirm:
                        return add_alliance, ctx, pnw, aptitle, monitor, sqlstr, existing, return_func
                    else:
                        return return_func, ctx, pnw

        else:
            embed = discord.Embed(
                title=aptitle + " - Add Alliance",
                description="I was unable to find that alliance! If the alliance was just created, give Requiem a "
                            "few minutes and try again! Would you like to try again?",
                colour=ctx.colour
            )
            message = await ctx.send(embed=embed)
            confirm = await reaction_factory.Confirminator(ctx, message).navigate()
            await clean_up(message)
            if confirm:
                return add_alliance, ctx, pnw, aptitle, monitor, sqlstr, existing, return_func
            else:
                return return_func, ctx, pnw
    else:
        embed = discord.Embed(
            title=aptitle + " - Add Alliance",
            description="There are already 15 alliances being monitored! If you wish to add another, you must remove "
                        "one!",
            colour=ctx.colour
        )
        message = await ctx.send(embed=embed)
        await asyncio.sleep(10)
        await clean_up(message)
        return return_func, ctx, pnw


async def set_alliance_channel(ctx, pnw, aptitle, monitor, sqlstr, existing, alliance, return_func):
    embed = discord.Embed(
        title=aptitle + " - Add Alliance",
        description="Alright! At this time, please say the name or id of the channel you wish to have "
                    f"**{alliance.name}'s** {monitor} notifications sent to! Otherwise, say 'cancel' to abort the "
                    "configuration!",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)

    try:
        response = await ctx.bot.wait_for("message", timeout=60, check=lambda m: m.author == ctx.author)
        await clean_up(message)
    except asyncio.TimeoutError:
        await clean_up(message)
        embed = discord.Embed(
            title=aptitle + " - Add Alliance",
            description="You timed out! Would you like to try again?",
            colour=ctx.colour
        )
        message = await ctx.send(embed=embed)
        confirm = await reaction_factory.Confirminator(ctx, message).navigate()
        await clean_up(message)
        if confirm:
            return set_alliance_channel, ctx, pnw, aptitle, monitor, sqlstr, existing, alliance, return_func
        else:
            return return_func, ctx, pnw

    if response.content.lower() == "cancel":
        return return_func, ctx, pnw

    try:
        channel = await commands.TextChannelConverter().convert(ctx, response.content)
        return set_alliance_role, ctx, pnw, aptitle, monitor, sqlstr, existing, alliance, channel, return_func

    except commands.BadArgument:
        embed = discord.Embed(
            title=aptitle + " - Add Alliance",
            description=f"Whoops! I wasn't able to find **{response.content}** in this guilds channels! "
                        "Would you like to try again?",
            colour=ctx.colour
        )
        message = await ctx.send(embed=embed)
        confirm = await reaction_factory.Confirminator(ctx, message).navigate()
        await clean_up(message)
        if confirm:
            return set_alliance_channel, ctx, pnw, aptitle, monitor, sqlstr, existing, alliance, return_func
        else:
            return return_func, ctx, pnw


async def set_alliance_role(ctx, pnw, aptitle, monitor, sqlstr, existing, alliance, channel, return_func):
    embed = discord.Embed(
        title=aptitle + " - Add Alliance",
        description=f"Alright! The channel **{channel.name}** will now be used for **{alliance.name}'s** "
                    f"{monitor} notifications!\n\n"
                    f"If you wish to have a role mentioned with **{alliance.name}** {monitor} notifications, please "
                    f"say its name or id now. Otherwise, say 'pass' to skip this step or 'cancel' to abort the "
                    f"configuration!",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)

    try:
        response = await ctx.bot.wait_for("message", timeout=60, check=lambda m: m.author == ctx.author)
        await clean_up(message)
    except asyncio.TimeoutError:
        await clean_up(message)
        embed = discord.Embed(
            title=aptitle + " - Add Alliance",
            description="You timed out! Would you like to try again?",
            colour=ctx.colour
        )
        message = await ctx.send(embed=embed)
        confirm = await reaction_factory.Confirminator(ctx, message).navigate()
        await clean_up(message)
        if confirm:
            return set_alliance_role, ctx, pnw, aptitle, monitor, sqlstr, existing, alliance, channel, return_func
        else:
            return return_func, ctx, pnw

    if response.content.lower() == "cancel":
        return return_func, ctx, pnw

    elif response.content.lower() == "pass":
        return save_alliance, ctx, pnw, aptitle, monitor, sqlstr, existing, alliance, channel, None, return_func

    try:
        role = await commands.RoleConverter().convert(ctx, response.content)
        return save_alliance, ctx, pnw, aptitle, monitor, sqlstr, existing, alliance, channel, role, return_func

    except commands.BadArgument:
        embed = discord.Embed(
            title=aptitle + " - Add Alliance",
            description=f"Whoops! I wasn't able to find **{response.content}** in this guilds roles! "
                        "Would you like to try again?",
            colour=ctx.colour
        )
        message = await ctx.send(embed=embed)
        confirm = await reaction_factory.Confirminator(ctx, message).navigate()
        await clean_up(message)
        if confirm:
            return set_alliance_role, ctx, pnw, aptitle, monitor, sqlstr, existing, alliance, channel, return_func
        else:
            return return_func, ctx, pnw


async def save_alliance(ctx, pnw, aptitle, monitor, sqlstr, existing, alliance, channel, role, return_func):
    embed = discord.Embed(
        title=aptitle + " - Add Alliance",
        description=f"Alright! The {monitor} notification configuration for **{alliance.name}** is now saved!",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    role_id = role.id if role else 0
    existing[f"{alliance.id}"] = {"channel": channel.id, "role": role_id}
    async with ctx.bot.pool.acquire() as conn:
        await conn.execute(f"UPDATE guilds SET {sqlstr} = ($1) WHERE guild_id = ($2)",
                           f"{existing}".replace("'", '"'), ctx.guild.id)
    await asyncio.sleep(10)
    await clean_up(message)
    return return_func, ctx, pnw
