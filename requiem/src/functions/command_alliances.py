# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import reaction_factory
import discord


def add_alliance_to_embed(alliance, embed):
    output = f"[{alliance.name}](https://politicsandwar.com/alliance/id={alliance.id})"
    if alliance.discord != "":
        output += f" - [Discord]({alliance.discord})"
    embed.add_field(
        name=alliance.rank,
        value=output,
        inline=False
    )


async def run_alliances(ctx):
    alliances = ctx.bot.pw.alliances_latest
    pages = []
    c = 0
    a = 1
    embed = discord.Embed(title=f"Alliances {a} - {a + 24}", colour=ctx.colour)

    for alliance in alliances[0:100]:
        if c < 10:
            c += 1
            add_alliance_to_embed(alliance, embed)

        else:
            pages.append(embed)
            a += 10
            c = 1
            embed = discord.Embed(title=f"Alliances {a} - {a + 24}", colour=ctx.colour)
            add_alliance_to_embed(alliance, embed)

    pages.append(embed)
    paginator = reaction_factory.Paginator(ctx.bot, ctx, ctx.author, pages)
    await paginator.navigate()
