# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
from discord.ext import commands
import asyncio
from src.functions import utils
from core.engine.utils import reaction_factory


async def run_leave(ctx):
    starter = main_menu, ctx
    await utils.runnable(starter)


def channel_config_string(ctx):
    channel = discord.utils.get(
        ctx.guild.channels, id=ctx.guild_config.leave_channel)
    if channel:
        channel_result = f"The server side farewell service is active! It is currently using {channel.mention}!"
    else:
        channel_result = "The server side farewell service is disabled!"
    return channel, channel_result


def message_config_string(ctx):
    message = ctx.guild_config.leave_message
    if message:
        message_result = f"A custom server side farewell message is set!"
    else:
        message_result = "The default server side farewell message is being used!"
        message = f"Welcome to the server **{ctx.author.name}**!"
    return message, message_result


def get_strings(ctx):
    a1, a2 = channel_config_string(ctx)
    b1, b2 = message_config_string(ctx)
    return a2, b2


async def main_menu(ctx):
    a2, b2 = get_strings(ctx)
    embed = discord.Embed(
        title="Farewell Service Configurator - Main Menu",
        description="Pick a setting to configure from the options below",
        color=ctx.colour
    )
    embed.add_field(name="📢 Announcement Channel", value=a2, inline=False)
    embed.add_field(name="🇦 Server Side Farewell Message", value=b2,
                    inline=False)
    message = await ctx.send(embed=embed)
    reacts = ['📢', '🇦', '❌']
    funcs = [conf_0_0, conf_1_0, None]
    menunator = reaction_factory.Menunator(ctx, message, funcs, reacts)
    result = await menunator.navigate()
    await utils.clean_up(message)
    return result, ctx


async def conf_0_0(ctx):
    channel, channel_result = channel_config_string(ctx)

    embed = discord.Embed(
        title="Farewell Service Configurator - Guild Side Announcer - Channel Configuration",
        description=f"{channel_result}\nWould you like to configure the member farewell channel?",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    confirminator = reaction_factory.Confirminator(ctx, message)
    confirm = await confirminator.navigate()
    await utils.clean_up(message)

    if confirm:
        return conf_0_1, ctx
    else:
        return main_menu, ctx


async def conf_0_1(ctx):
    channel, channel_result = channel_config_string(ctx)

    if channel:
        descrip = "Otherwise say 'cancel' to abort the configuration or 'clear' to clear your current configuration."
    else:
        descrip = "Otherwise, say 'cancel' to abort the configuration."

    embed = discord.Embed(
        title="Farewell Service Configurator - Guild Side Announcer - Channel Configuration",
        description="Alright! At this time, please say the name or id of the channel you wish to "
                    f"use for member farewells! {descrip}",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    try:
        response = await ctx.bot.wait_for("message", timeout=60, check=lambda m: m.author == ctx.author)
        await utils.clean_up(message)
        return conf_0_2, ctx, response.content
    except asyncio.TimeoutError:
        await utils.clean_up(message)
        embed = discord.Embed(
            title="Farewell Service Configurator - Guild Side Announcer - Channel Configuration",
            description="You timed out! Would you like to try again?",
            colour=ctx.colour
        )
        message = await ctx.send(embed=embed)
        confirm = await reaction_factory.Confirminator(ctx, message).navigate()
        await utils.clean_up(message)
        if confirm:
            return conf_0_1, ctx, response
        else:
            return main_menu, ctx


async def conf_0_2(ctx, response):
    channel, channel_result = channel_config_string(ctx)
    try_again = False

    if response == "clear":
        if channel:
            output = "Alright! Your current configuration has been cleared!"
            async with ctx.bot.pool.acquire() as conn:
                await conn.execute("UPDATE guilds SET leave_channel = ($1) WHERE guild_id = ($2)", 0, ctx.guild.id)
        else:
            output = "The service is not currently configured! Would you like to try again?"
            try_again = True

    elif response in ('cancel', 'abort'):
        output = "Alright! Your current configuration has not been changed!"

    else:
        try:
            channel = await commands.TextChannelConverter().convert(ctx, response)
            output = f"Alright! The channel **{channel.name}** will now be used for member farewell announcements!"
        except commands.BadArgument:
            output = f"Whoops! I wasn't able to find **{response}** in this guilds channels! " \
                     "Would you like to try again?"
            try_again = True
        async with ctx.bot.pool.acquire() as conn:
            await conn.execute("UPDATE guilds SET leave_channel = ($1) WHERE guild_id = ($2)", channel.id, ctx.guild.id)

    embed = discord.Embed(
        title="Farewell Service Configurator - Guild Side Announcer - Channel Configuration",
        description=output,
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)

    if try_again:
        confirminator = reaction_factory.Confirminator(ctx, message)
        confirm = await confirminator.navigate()
        await utils.clean_up(message)

        if confirm:
            return conf_0_1, ctx
        else:
            return main_menu, ctx

    await asyncio.sleep(10)
    await utils.clean_up(message)
    return main_menu, ctx


async def conf_1_0(ctx):
    message, message_result = message_config_string(ctx)

    embed = discord.Embed(
        title="Farewell Service Configurator - Guild Side Announcer - Message Configuration",
        description=f"{message_result}\nWould you like to configure the member farewell message?",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    confirminator = reaction_factory.Confirminator(ctx, message)
    confirm = await confirminator.navigate()
    await utils.clean_up(message)

    if confirm:
        return conf_1_1, ctx
    else:
        return main_menu, ctx


async def conf_1_1(ctx):
    message, message_result = message_config_string(ctx)

    if message:
        descrip = "Otherwise say 'cancel' to abort the configuration or 'clear' to clear your current configuration."
    else:
        descrip = "Otherwise, say 'cancel' to abort the configuration."

    embed = discord.Embed(
        title="Farewell Service Configurator - Guild Side Announcer - Message Configuration",
        description=f"Alright! At this time, please say the message you wish to use for member farewell! {descrip}",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    try:
        response = await ctx.bot.wait_for("message", timeout=60, check=lambda m: m.author == ctx.author)
        await utils.clean_up(message)
        return conf_1_2, ctx, response.content
    except asyncio.TimeoutError:
        await utils.clean_up(message)
        embed = discord.Embed(
            title="Farewell Service Configurator - Guild Side Announcer - Message Configuration",
            description="You timed out! Would you like to try again?",
            colour=ctx.colour
        )
        message = await ctx.send(embed=embed)
        confirm = await reaction_factory.Confirminator(ctx, message).navigate()
        await utils.clean_up(message)
        if confirm:
            return conf_1_1, ctx
        else:
            return main_menu, ctx


async def conf_1_2(ctx, response):
    message, message_result = message_config_string(ctx)
    try_again = False

    if response == "clear":
        if message:
            output = "Alright! Your current configuration has been cleared!"
            async with ctx.bot.pool.acquire() as conn:
                await conn.execute("UPDATE guilds SET leave_message = ($1) WHERE guild_id = ($2)", None, ctx.guild.id)
        else:
            output = "There is not currently a custom message set! Would you like to set one?"
            try_again = True

    elif response in ('cancel', 'abort'):
        output = "Alright! Your current configuration has not been changed!"

    else:
        if len(response) < 1500:
            output = f"{response}\n\nDoes this look good?"
            embed = discord.Embed(
                title="Farewell Service Configurator - Guild Side Announcer - Message Configuration",
                description=output,
                colour=ctx.colour
            )
            message = await ctx.send(embed=embed)

            confirminator = reaction_factory.Confirminator(ctx, message)
            confirm = await confirminator.navigate()
            await utils.clean_up(message)

            if confirm:
                async with ctx.bot.pool.acquire() as conn:
                    await conn.execute("UPDATE guilds SET leave_message = ($1) WHERE guild_id = ($2)", response,
                                       ctx.guild.id)
                output = "Alright! I'll use this as the farewell message from now on!"
            else:
                output = "Alright! Would you like to try again?"
                try_again = True

        else:
            output = "Whoops! The message you provided was too long! Please limit your farewell to 1500 " \
                     "characters or less! Would you like to try again?"
            try_again = True

    embed = discord.Embed(
        title="Farewell Service Configurator - Guild Side Announcer - Message Configuration",
        description=output,
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)

    if try_again:
        confirminator = reaction_factory.Confirminator(ctx, message)
        confirm = await confirminator.navigate()
        await utils.clean_up(message)

        if confirm:
            return conf_1_1, ctx
        else:
            return main_menu, ctx

    await asyncio.sleep(10)
    await utils.clean_up(message)
    return main_menu, ctx
