
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
import json


async def run_react_to_tag(self, message):
	if message.author.bot:
		return
		
	if message.guild:
		config = self.bot.database.get_guild_config(message.guild.id)
		existing = json.loads(config.tags)
		key = message.content.replace(" ", "_")

		if key in existing.keys():
			try:
				await message.channel.send(existing[key].replace("$@", "'"))
			except (discord.NotFound, discord.Forbidden):
				pass