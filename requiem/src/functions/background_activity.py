# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord, random, asyncio


playing = [
    "With the humans",
    "Minecraft",
    "With the elements",
    "Purple Butterfly on Your Right Shoulder",
    "Battlefield 4"
]

watching = [
    "The world burn",
    "Boki sleep",
    "Verin code... *cringe*",
    "Solar bully Verin",
    "Python 101",
    "Beep Beep I'm A Sheep",
    "the chemicals in the water turn the freakin frogs gay"
]


async def run_activity(self):
    try:
        activities = [
            discord.Activity(type=0, name=random.choice(playing)),
            discord.Activity(type=3, name=random.choice(watching)),
            discord.Activity(type=2, name=f"{len(self.bot.users)} users")
        ]
        while True:
            await self.bot.change_presence(activity=random.choice(activities))
            await asyncio.sleep(300)
    except asyncio.CancelledError:
        return
