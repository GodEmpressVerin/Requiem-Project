# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from pwpy import exceptions, scrape
import discord


def get_leader(ctx, nation):
    try:
        conf = ctx.get_user_config
        leader = [u for u in ctx.bot.users if conf(
            u.id).nation_id == nation.id][0].mention
    except (IndexError, AttributeError):
        leader = f"[{nation.leader}](https://politicsandwar.com/inbox/message/" \
                 f"receiver={nation.leader.replace(' ', '%20')})"
    return leader


async def run_war(ctx, target):
    if ctx.bot.pw.wars_latest and ctx.bot.pw.nations_latest:
        try:
            war = await ctx.bot.pw.war(target)
            agg_nation = await scrape.lookup(ctx.bot.pw.nations_latest, id=war.aggressor_id)
            def_nation = await scrape.lookup(ctx.bot.pw.nations_latest, id=war.defender_id)

            embed = discord.Embed(
                colour=ctx.colour
            )
            if war.war_ended:
                embed.add_field(
                    name="War Has Ended!", value="This data is representative of the wars final state!")
            embed.add_field(name="Attacking Nation ---",
                            value=f"[{agg_nation.name}](https://politicsandwar.com/nation/id={agg_nation.id})",
                            inline=False)
            embed.add_field(name="Leader", value=get_leader(ctx, agg_nation))
            if agg_nation.alliance_id != 0:
                alliance = f"[{agg_nation.alliance}](https://politicsandwar.com/alliance/id={agg_nation.alliance_id})"
                embed.add_field(name="Alliance", value=alliance)
            embed.add_field(name="Military Action Points", value=war.agg_map)
            embed.add_field(name="Is Fortified", value=war.agg_fort)
            embed.add_field(name="Resistance", value=war.agg_res)
            embed.add_field(name="Infra Lost", value=f'{war.agg_infra_lost:,}')
            embed.add_field(name="Money Lost", value=f'{war.agg_money_lost:,}')
            embed.add_field(name="Soldiers Lost",
                            value=f'{war.agg_soldiers_lost:,}')
            embed.add_field(name="Tanks Lost", value=f'{war.agg_tanks_lost:,}')
            embed.add_field(name="Aircraft Lost",
                            value=f'{war.agg_aircraft_lost:,}')
            embed.add_field(name="Ships Lost", value=f'{war.agg_ships_lost:,}')

            embed.add_field(name="Defending Nation ---",
                            value=f"[{def_nation.name}](https://politicsandwar.com/nation/id={def_nation.id})",
                            inline=False)
            embed.add_field(name="Leader", value=get_leader(ctx, def_nation))
            if def_nation.alliance_id != 0:
                alliance = f"[{def_nation.alliance}](https://politicsandwar.com/alliance/id={def_nation.alliance_id})"
                embed.add_field(name="Alliance", value=alliance)
            embed.add_field(name="Military Action Points", value=war.def_map)
            embed.add_field(name="Is Fortified", value=war.def_fort)
            embed.add_field(name="Resistance", value=war.def_res)
            embed.add_field(name="Infra Lost", value=f'{war.def_infra_lost:,}')
            embed.add_field(name="Money Lost", value=f'{war.def_money_lost:,}')
            embed.add_field(name="Soldiers Lost",
                            value=f'{war.def_soldiers_lost:,}')
            embed.add_field(name="Tanks Lost", value=f'{war.def_tanks_lost:,}')
            embed.add_field(name="Aircraft Lost",
                            value=f'{war.def_aircraft_lost:,}')
            embed.add_field(name="Ships Lost", value=f'{war.def_ships_lost:,}')

        except exceptions.InvalidTarget:
            embed = discord.Embed(
                title="No Nation Found",
                description="I was unable to find a nation matching the given input!"
                            " Please check your input and try again!",
                colour=ctx.colour
            )
        except RuntimeError:
            embed = discord.Embed(
                title="Response Read Error",
                description="The API returned a broken response! Please try again later...",
                colour=ctx.colour
            )
    else:
        embed = discord.Embed(
            title="PWPY is not ready yet!",
            description="PWPY has not finished caching wars and nations! Please wait a few minutes and try again!",
            colour=ctx.colour
        )
    await ctx.send(embed=embed)
