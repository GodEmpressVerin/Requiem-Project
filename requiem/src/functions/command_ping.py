# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
import time
import asyncio


async def run_ping(ctx):
    embed = discord.Embed(description='Pinging...', color=ctx.colour)
    start = time.monotonic()
    message = await ctx.send(embed=embed)

    millis = (time.monotonic() - start) * 1000
    heartbeat = ctx.bot.latency * 1000

    if heartbeat >= 140:
        color = discord.Colour.red()
    elif heartbeat > 70 < 140:
        color = discord.Colour.gold()
    else:
        color = discord.Colour.green()

    embed = discord.Embed(color=color)
    embed.add_field(name="Heartbeat", value=f"{int(heartbeat)}ms")
    embed.add_field(name="ACK", value=f"{int(millis)}ms")
    await asyncio.sleep(1)
    await message.edit(embed=embed)
