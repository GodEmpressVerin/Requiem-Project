# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
import asyncio
from core.engine.utils import extra_funcs


async def set_colour_config(ctx, colour):
    async with ctx.bot.pool.acquire() as conn:
        await conn.execute("UPDATE guilds SET embed_colour = ($1) WHERE guild_id = ($2)", colour, ctx.guild.id)


async def run_setcolour(ctx, colour):
    async with ctx.typing():

        verified_colour = extra_funcs.get_colour(colour)

        if verified_colour:
            output = f"Alright! Embeds will now be sent in the colour **{colour}**!"
            await set_colour_config(ctx, colour)

        else:
            colour_list = [colour.title() for colour in extra_funcs.get_colour("list")]
            fmt = f'{", ".join(colour_list)[:-1]}, and {colour_list[-1]}'
            output = f"The colour specified is not available! The following colours are available to choose from: {fmt}"
            await set_colour_config(ctx, "purple")

        await asyncio.sleep(1.5)
        embed = discord.Embed(
            title="Colour Configuration",
            description=output,
            colour=ctx.colour
        )
        await ctx.send(embed=embed)
