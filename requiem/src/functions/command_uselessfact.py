# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord


async def run_uselessfact(ctx):
    async with ctx.bot.csess.get("https://uselessfacts.jsph.pl/random.json?language=en") as session:
        data = await session.json(content_type="application/json")
    fact, source_url, source = data["text"], data["source_url"], data["source"]

    embed = discord.Embed(colour=ctx.colour)
    embed.add_field(name="Fact", value=fact, inline=False)
    embed.add_field(name="Source", value=f"[{source}]({source_url})", inline=False)
    embed.set_footer(text="Provided By uselessfacts.jsph.pl")
    await ctx.send(embed=embed)
