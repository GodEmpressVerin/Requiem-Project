# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
from core.engine.utils import reaction_factory
from discord.ext import commands
from src.functions import utils
import asyncio


async def set_autorole(ctx, role):
    async with ctx.bot.pool.acquire() as conn:
        await conn.execute("UPDATE guilds SET auto_role = ($1) WHERE guild_id = ($2)", role, ctx.guild.id)


def find_role(ctx, target):
    check_role = None
    for role in ctx.guild.roles:
        if role.name.lower() == target.lower():
            check_role = role
    return check_role


async def run_autorole(ctx):
    starter = main_menu, ctx
    await utils.runnable(starter)


def role_check_string(ctx):
    role_check = discord.utils.get(ctx.guild.roles, id=ctx.guild_config.auto_role)
    if role_check:
        output = f"The role **{role_check.name}** is currently being used for automatic role assignment!"
    else:
        output = "There is not currently a role configured for automatic role assignment!"
    return role_check, output


async def main_menu(ctx):
    role, output = role_check_string(ctx)

    embed = discord.Embed(
        title="Automatic Role Assignment",
        description=f"Pick a setting to configure from the options below",
        colour=ctx.colour
    )
    embed.add_field(name="✅ Role", value=output)
    embed.add_field(name="🔧 No Role Correct", value="Runs through the members lists and ensures anyone who doesn't have"
                                                    "a role gets your configured base role.")
    message = await ctx.send(embed=embed)
    reacts = ['✅', '🔧', '❌']
    funcs = [conf_0_0, conf_1_0, None]
    menunator = reaction_factory.Menunator(ctx, message, funcs, reacts)
    result = await menunator.navigate()
    await utils.clean_up(message)
    return result, ctx


async def conf_0_0(ctx):
    role, role_string = role_check_string(ctx)

    embed = discord.Embed(
        title="Automatic Role Assignment - Role Configuration",
        description=f"{role_string}\nWould you like to configure the role for automatic role assignment?",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    confirminator = reaction_factory.Confirminator(ctx, message)
    confirm = await confirminator.navigate()
    await utils.clean_up(message)

    if confirm:
        return conf_0_1, ctx
    else:
        return main_menu, ctx


async def conf_0_1(ctx):
    role, role_string = role_check_string(ctx)

    if role:
        descrip = "Otherwise say 'cancel' to abort the configuration or 'clear' to clear your current configuration."
    else:
        descrip = "Otherwise, say 'cancel' to abort the configuration."

    embed = discord.Embed(
        title="Automatic Role Assignment - Role Configuration",
        description="Alright! At this time, please say the name or id of the role you wish to "
                    f"use for automatic role assignment! {descrip}",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    try:
        response = await ctx.bot.wait_for("message", timeout=60, check=lambda m: m.author == ctx.author)
        await utils.clean_up(message)
        return conf_0_2, ctx, response.content
    except asyncio.TimeoutError:
        await utils.clean_up(message)
        return utils.handle_timeout, ctx, conf_0_1, main_menu, \
            "Automatic Role Assignment - Role Configuration"


async def conf_0_2(ctx, response):
    role, role_string = role_check_string(ctx)
    try_again = False

    if response == "clear":
        if role:
            output = "Alright! Your current configuration has been cleared!"
            async with ctx.bot.pool.acquire() as conn:
                await conn.execute("UPDATE guilds SET auto_role = ($1) WHERE guild_id = ($2)", 0, ctx.guild.id)
        else:
            output = "The service is not currently configured! Would you like to try again?"
            try_again = True

    elif response in ('cancel', 'abort'):
        output = "Alright! Your current configuration has not been changed!"

    else:
        role = await commands.RoleConverter().convert(ctx, response)
        if role:
            output = f"Alright! The role **{role.name}** will now be used for member greeting announcements!"
        else:
            output = f"Whoops! I wasn't able to find **{response}** in this guilds roles! " \
                     "Would you like to try again?"
            try_again = True
        async with ctx.bot.pool.acquire() as conn:
            await conn.execute("UPDATE guilds SET auto_role = ($1) WHERE guild_id = ($2)", role.id, ctx.guild.id)

    embed = discord.Embed(
        title="Automatic Role Assignment - Role Configuration",
        description=output,
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)

    if try_again:
        confirminator = reaction_factory.Confirminator(ctx, message)
        confirm = await confirminator.navigate()
        await utils.clean_up(message)

        if confirm:
            return conf_0_1, ctx
        else:
            return main_menu, ctx

    await asyncio.sleep(10)
    await utils.clean_up(message)
    return main_menu, ctx


async def conf_1_0(ctx):
    role, role_string = role_check_string(ctx)
    move_on = False

    if role:
        nru = [user for user in ctx.guild.members if role not in user.roles and len(user.roles) == 1]
        if len(nru) > 0:
            output = f"There are {len(nru)} users who do not have a role! Would you like to run role correction now?"
            move_on = True
        else:
            output = "There are no users who need to have their roles corrected!"
    else:
        output = "You must configure a role for automatic role assignment before you can run role correction!"

    embed = discord.Embed(
        title="Automatic Role Assignment - No-Role Correction",
        description=output,
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    if move_on:
        confirminator = reaction_factory.Confirminator(ctx, message)
        confirm = await confirminator.navigate()
        await utils.clean_up(message)

        if confirm:
            return conf_1_1, ctx
    else:
        await asyncio.sleep(10)
    return main_menu, ctx


async def conf_1_1(ctx):
    role, role_string = role_check_string(ctx)
    reason = f"Automatic Role Correction | Ran by {ctx.author.name}"

    corrected = [user.add_roles(role, reason=reason) for user in ctx.guild.members
                 if role not in user.roles and len(user.roles) == 1]
    embed = discord.Embed(
        title="Automatic Role Assignment - No-Role Correction",
        description=f"{len(corrected)} users had their roles corrected!",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    await asyncio.sleep(10)
    await utils.clean_up(message)
    return main_menu, ctx
