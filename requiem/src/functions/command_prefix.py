# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord


async def set_prefix(ctx, prefix):
    async with ctx.bot.pool.acquire() as conn:
        await conn.execute("UPDATE guilds SET prefix = ($1) WHERE guild_id = ($2)", prefix, ctx.guild.id)


async def run_prefix(ctx, prefix):

    if prefix:
        if len(prefix) <= 5:
            await set_prefix(ctx, prefix)
            output = f'Alright! This guilds prefix has been set to "{prefix}"!'
        else:
            output = "Sorry, but the prefix given was too long! Please keep it to 4 characters or less!"

    else:
        prefix = ctx.bot.config.prefix
        await set_prefix(ctx, prefix)
        output = f'Alright! This guilds prefix has been set to the default of "{prefix}"!'

    embed = discord.Embed(
        title="Prefix Configuration",
        description=output,
        colour=ctx.colour
    )
    await ctx.send(embed=embed)
