# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
import time


async def main_site_test(bot, embed):
    start = time.time()
    try:
        async with bot.csess.get("https://politicsandwar.com/") as response:
            end = time.time()
            output = f"The site responded in {int(end-start)} seconds with a code of {response.status}"
    except:
        output = "There was an unexpected error while connecting to the site!"
    embed.add_field(name="Main Site", value=output, inline=False)


async def test(api, name, embed):
    try:
        start = time.time()
        try:
            await api()
            end = time.time()
            output = f"Ping: {int(end-start)} seconds"
        except RuntimeError:
            end = time.time()
            output = f"Ping: {int(end-start)} seconds | There was an issue reading the returned result."
    except:
        output = f"Requiem was unable to connect to {name}!"

    embed.add_field(name=name, value=output, inline=False)


async def run_pwping(ctx):
    pnw = ctx.bot.pw

    embed = discord.Embed(
        title="PnW Ping Test",
        description="This may take a few minutes! I'll be typing in this channel until all responses are returned!",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)

    embed = discord.Embed(
        title="PnW Ping Test",
        description="Tests for the Politics and War Site and API",
        colour=ctx.colour
    )

    async with ctx.typing():
        await test(pnw.nations, "Nations", embed)
        await test(pnw.alliances, "Alliances", embed)
        await test(pnw.wars, "Wars", embed)
        await main_site_test(ctx.bot, embed)

    try:
        await message.delete()
    except (discord.NotFound, discord.Forbidden):
        pass

    await ctx.send(embed=embed)



