# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import extra_funcs
import discord
import json


async def send_applicant_notification(bot, guild, channel, notif_role, colour, nation):
    if channel:
        try:
            conf = bot.database.get_user_config
            leader = [u for u in guild.members if conf(u.id) and conf(u.id).nation_id == nation.id][0].mention
        except (IndexError, AttributeError):
            leader = f"[{nation.leader}](https://politicsandwar.com/inbox/message/" \
                f"receiver={nation.leader.replace(' ', '%20')})"
        alliance = f"[{nation.alliance}](https://politicsandwar.com/alliance/id={nation.alliance_id})"
        embed = discord.Embed(
            description=f"{alliance} - New Member Application",
            colour=colour
        )
        embed.add_field(name="Nation", value=f"[{nation.name}](https://politicsandwar.com/nation/id={nation.id})")
        embed.add_field(name="Leader", value=leader)
        embed.add_field(name="Color", value=nation.color)
        embed.add_field(name="Cities", value=nation.cities)
        embed.add_field(name="Offensive Wars", value=nation.offensive_wars)
        embed.add_field(name="Defensive Wars", value=nation.defensive_wars)

        try:
            if notif_role:
                await channel.send(notif_role.mention)
            await channel.send(embed=embed)
        except (discord.Forbidden, discord.HTTPException):
            pass


async def run_applicant_monitor(self, applicant):
    for guild in self.bot.guilds:
        config = self.bot.get_guild_config(guild.id)
        if config:
            colour = extra_funcs.get_colour(config.embed_colour)
            applicant_monitor = json.loads(config.applicant_monitor)
            if str(applicant.alliance_id) in applicant_monitor.keys():
                channel, role = applicant_monitor[str(applicant.alliance_id)].values()
                role = discord.utils.get(guild.roles, id=role)
                channel = discord.utils.get(guild.channels, id=channel)
                await send_applicant_notification(self.bot, guild, channel, role, colour, applicant)
