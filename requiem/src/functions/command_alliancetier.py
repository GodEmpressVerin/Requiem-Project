# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import reaction_factory
from pwpy import scrape
import discord


async def run_alliancetier(ctx, target):
    pages = []
    tier = None

    if "-tier" in target:
        tier = target.split("-tier", 1)[1][1:]
        target = target.split("-tier", 1)[0][:-1]

    if ctx.bot.pw.alliances_latest and ctx.bot.pw.nations_latest:
        alliance = await scrape.lookup(ctx.bot.pw.alliances_latest, id=target, name=target)
        if alliance:
            aastr = f"[{alliance.name} - {alliance.acronym}](https://politicsandwar.com/alliance/id={alliance.id})"
            tiering = await scrape.tier(ctx.bot.pw.nations_latest, alliance.id)

            if tier:

                try:
                    tier = int(tier)
                    if tier in tiering.keys():
                        embed = discord.Embed(title=f"City Tiering - {tier} cities", description=aastr,
                                              colour=ctx.colour)
                        nations = tiering[tier]
                        c = 0
                        nc = 0

                        for nation in sorted(nations, key=lambda n: n.score):

                            c += 1
                            nc += 1

                            if c <= 25:
                                embed.add_field(
                                    name=f"Nation {nc} of {len(nations)}",
                                    value=f"[{nation.name}](https://politicsandwar.com/alliance/id={nation.id})",
                                    inline=False)
                            else:
                                pages.append(embed)
                                embed = discord.Embed(title=f"City Tiering - {tier} cities", description=aastr,
                                                      colour=ctx.colour)
                                embed.add_field(
                                    name=f"Nation {nc} of {len(nations)}",
                                    value=f"[{nation.name}](https://politicsandwar.com/alliance/id={nation.id})",
                                    inline=False)
                                c = 1
                        if embed not in pages:
                            pages.append(embed)

                    else:
                        embed = discord.Embed(
                            title="Invalid City Count",
                            description=f"There are no nations in that alliance with {tier} cities!",
                            colour=ctx.colour
                        )
                        pages.append(embed)

                except ValueError:
                    embed = discord.Embed(
                        title="Invalid City Count",
                        description=f"That was not a number! Please keep your specified tier to integer only!",
                        colour=ctx.colour
                    )
                    pages.append(embed)

            else:
                embed = discord.Embed(
                    title=f"City Tiering", description=aastr, colour=ctx.colour)
                c = 0
                for tier, nations in sorted(tiering.items()):
                    c += 1

                    if c <= 25:
                        embed.add_field(
                            name=f"{tier} Cities", value=f"{len(nations)}")
                    else:
                        pages.append(embed)
                        embed = discord.Embed(
                            title="City Tiering", description=aastr, colour=ctx.colour)
                        embed.add_field(
                            name=f"{tier} Cities", value=f"{len(nations)}")
                        c = 1
                if embed not in pages:
                    pages.append(embed)

        else:
            embed = discord.Embed(
                title="No Alliance Found",
                description="I was unable to find an alliance matching the given input!"
                            " Please check your input and try again!",
                colour=ctx.colour
            )
            pages.append(embed)

    else:
        embed = discord.Embed(
            title="PWPY is not ready yet!",
            description="PWPY has not finished caching nations and alliances! Please wait a few minutes and try again!",
            colour=ctx.colour
        )
        pages.append(embed)

    paginator = reaction_factory.Paginator(ctx.bot, ctx, ctx.author, pages)
    await paginator.navigate()
