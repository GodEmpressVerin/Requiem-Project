# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import asyncio


async def run_pingcheck(self):
    try:
        avg = 0
        count = 0
        pa = 1
        while True:

            ping_time = int(self.bot.latency*1000)
            while ping_time < 0:
                ping_time = int(self.bot.latency*1000)
                await asyncio.sleep(1)

            avg = ping_time + avg // pa
            if ping_time > 100:
                output = "Websocket Ping Check - HIGH PING ALERT - "
            else:
                output = "Websocket Ping Check - "

            count += 1
            pa += 1
            self.logger.info(output + f"Average Ping {avg} | Current Ping {ping_time}")
            await asyncio.sleep(3600)
    except asyncio.CancelledError:
        return