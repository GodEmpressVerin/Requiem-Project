# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from pwpy import calculate
import discord


async def run_infra(ctx, start, end, args):
    try:
        value = calculate.infra(start, end)
        multiplier = 1.00

        if "urban" in args:
            multiplier = multiplier - .05
        if "cce" in args:
            multiplier = multiplier - .05

        value = value * multiplier

        embed = discord.Embed(
            title="Infra Calculator",
            description=f"From {start} to {end} Infra",
            colour=ctx.colour
        )
        embed.add_field(name="Starting Infra Amount",
                        value=f"{start:,.2f}", inline=True)
        embed.add_field(name="Finishing Infra Amount",
                        value=f"{end:,.2f}", inline=True)
        embed.add_field(name="Total Cost",
                        value=f"${value:,.2f}", inline=False)

    except ValueError:
        embed = discord.Embed(
            title="Infra Calculator",
            description=f"You passed an invalid value! Please note that this calculator is limited to a max of 10000 "
                        f"infra and can not calculate infra delete payback!",
            colour=ctx.colour
        )

    await ctx.send(embed=embed)
