# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
from core.engine.utils import extra_funcs


def parse_message(message, member):
    message = message.replace("%user%", f"{member.mention}").replace(
        f"%members%", f"{len(member.guild.members)}").replace(
        f"%humans%",
        f"{len([user for user in member.guild.members if user.bot is False])}").replace(
        f"%bots%", f"{len([user for user in member.guild.members if user.bot is True])}").replace(
        f"%user_nomen%", f"**{member.name}**").replace(
        f"%guild%", f"**{member.guild.name}**"
    )
    return message


async def run_announce_member_left(self, member):
    guild = member.guild
    conf = self.bot.get_guild_config(guild.id)

    if conf:
        leave_channel = discord.utils.get(guild.channels, id=conf.leave_channel)
        colour = extra_funcs.get_colour(conf.embed_colour)

        if leave_channel:
            msg = parse_message(
                conf.leave_message if conf.leave_message else "%user_nomen% has left the guild!", member)

            embed = discord.Embed(description=msg, colour=colour)
            try:
                await leave_channel.send(embed=embed)
            except (discord.Forbidden, discord.NotFound):
                pass
