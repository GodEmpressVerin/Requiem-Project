# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from pwpy import exceptions
import discord


async def run_alliance(ctx, target):
    if ctx.bot.pw.alliances_latest:
        try:
            alliance = await ctx.bot.pw.alliance(target)

            desc = []
            if alliance.discord != "":
                desc.append(f"[Discord]({alliance.discord})")
            if alliance.forum != "":
                desc.append(f"[Forum]({alliance.forum})")
            desc = " - ".join(desc) if len(desc) > 0 else None

            embed = discord.Embed(
                title=f"{alliance.name} - {alliance.acronym}",
                description=desc,
                url=f"https://politicsandwar.com/alliance/id={alliance.id}",
                colour=ctx.colour
            )
            embed.add_field(name="Score", value=f'{alliance.score:,}')
            embed.add_field(name="Color", value=alliance.color.title())
            embed.add_field(name="Accepting Members",
                            value=str(alliance.accepting_members))
            embed.add_field(name="Cities", value=f'{alliance.cities:,}')
            embed.add_field(name="Soldiers", value=f'{alliance.soldiers:,}')
            embed.add_field(name="Tanks", value=f'{alliance.tanks:,}')
            embed.add_field(name="Aircraft", value=f'{alliance.aircraft:,}')
            embed.add_field(name="Ships", value=f'{alliance.ships:,}')
            embed.add_field(name="Missiles", value=f'{alliance.missiles:,}')
            embed.add_field(name="Nukes", value=f'{alliance.nukes:,}')
            embed.add_field(name="Treasures", value=f'{alliance.treasures:,}')
            embed.set_image(url=alliance.flag)

        except exceptions.InvalidTarget:
            embed = discord.Embed(
                title="No Alliance Found",
                description="I was unable to find an alliance matching the given input!"
                            " Please check your input and try again!",
                colour=ctx.colour
            )
        except RuntimeError:
            embed = discord.Embed(
                title="Response Read Error",
                description="The API returned a broken response! Please try again later...",
                colour=ctx.colour
            )

    else:
        embed = discord.Embed(
            title="PWPY is not ready yet!",
            description="PWPY has not finished caching alliances! Please wait a few minutes and try again!",
            colour=ctx.colour
        )

    await ctx.send(embed=embed)
