# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
import random


async def run_dirtyneko(ctx):
    choices = [
        "feet", "yuri", "trap", "futanari", "hololewd", "lewdkemo", "solog",
        "feetg", "cum", "erokemo", "les", "lewdk", "lewd", "eroyuri", "eron",
        "cum_jpg", "bj", "nsfw_neko_gif", "solo", "nsfw_avatar", "anal", "hentai",
        "erofeet", "holo", "keta", "blowjob", "pussy", "tits", "holoero", "pussy_jpg",
        "pwankg", "classic", "kuni", "erok", "boobs", "Random_hentai_gif", "ero"
    ]
    choice = random.choice(choices)
    async with ctx.bot.csess.get(f"https://nekos.life/api/v2/img/{choice}") as session:
        data = await session.json(content_type="application/json")
    url = data["url"]

    embed = discord.Embed(color=ctx.colour)
    embed.set_image(url=url)
    embed.set_footer(text="Results provided by https://Nekos.Life")
    await ctx.send(embed=embed)
