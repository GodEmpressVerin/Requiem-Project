# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from pwpy import calculate
from core.engine.utils import extra_funcs
import discord
import json


async def send_beige_notification(bot, guild, channel, notif_role, colour, nation):
    if channel:
        att_max, att_min, def_max, def_min = calculate.score_range(
            nation.score)
        try:
            conf = bot.database.get_user_config
            leader = [u for u in guild.members if conf(u.id) and conf(u.id).nation_id == nation.id][0].mention
        except (IndexError, AttributeError):
            leader = f"[{nation.leader}](https://politicsandwar.com/inbox/message/" \
                f"receiver={nation.leader.replace(' ', '%20')})"
        alliance = f"[{nation.alliance}](https://politicsandwar.com/alliance/id={nation.alliance_id})"
        embed = discord.Embed(
            description=f"{alliance} - A nation has left beige",
            colour=colour
        )
        embed.add_field(
            name="Nation", value=f"[{nation.name}](https://politicsandwar.com/nation/id={nation.id})")
        embed.add_field(name="Leader", value=leader)
        embed.add_field(name="Score", value=f'{nation.score:,}')
        embed.add_field(name="Defensive Range",
                        value=f"{int(def_min):,} - {int(def_max):,}")
        embed.add_field(name="Offensive Range",
                        value=f"{int(att_min):,} - {int(att_max):,}")
        try:
            if notif_role:
                await channel.send(notif_role.mention)
            await channel.send(embed=embed)
        except (discord.Forbidden, discord.HTTPException):
            pass


async def run_beige_monitor(self, nation):
    for guild in self.bot.guilds:
        config = self.bot.get_guild_config(guild.id)
        if config:
            colour = extra_funcs.get_colour(config.embed_colour)
            beige_monitor = json.loads(config.beige_monitor)
            if str(nation.alliance_id) in beige_monitor:
                channel, role = beige_monitor[str(nation.alliance_id)].values()
                role = discord.utils.get(guild.roles, id=role)
                channel = discord.utils.get(guild.channels, id=channel)
                await send_beige_notification(self.bot, guild, channel, role, colour, nation)
