
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import objects
from discord.ext import commands
from src.functions import command_help


class Help(objects.Cog):
    """! Help Command Module for Cardinal"""
    def __init__(self, bot):
        self.bot = bot
        self.name = "Help"

    @commands.command(brief="Provides a list of commands.")
    async def help(self, ctx, *, module=None):
        """Gives a list of commands, descriptions, and their usage."""
        await command_help.run_help(ctx, module)


def setup(bot):
    bot.add_cog(Help(bot))
