"""
Please understand Music bots are complex, and that even this basic example can be daunting to a beginner.

For this reason it's highly advised you familiarize yourself with discord.py, python and asyncio, BEFORE
you attempt to write a music bot.

This example makes use of: Python 3.6

For a more basic voice example please read:
    https://github.com/Rapptz/discord.py/blob/rewrite/examples/basic_voice.py

This is a very basic playlist example, which allows per guild playback of unique queues.
The commands implement very basic logic for basic usage. But allow for expansion. It would be advisable to implement
your own permissions and usage logic for commands.

e.g You might like to implement a vote before skipping the song or only allow admins to stop the player.

Music bots require lots of work, and tuning. Goodluck.
If you find any bugs feel free to ping me on discord. @Eviee#0666
"""
import discord
from discord.ext import commands
from core.engine.utils import reaction_factory

import asyncio
from async_timeout import timeout
from functools import partial
from youtube_dl import YoutubeDL
import youtube_dl
import os
from core.engine.utils import objects


ytdlopts = {
    'format': 'bestaudio/best',
    'outtmpl': f'core/data/cache/ytdl_downloads/%(autonumber)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0',  # ipv6 addresses cause issues sometimes
}

ffmpegopts = {
    'before_options': '-nostdin',
    'options': '-vn'
}

ytdl = YoutubeDL(ytdlopts)


class VoiceConnectionError(commands.CommandError):
    """Custom Exception class for connection errors."""


class InvalidVoiceChannel(VoiceConnectionError):
    """Exception for cases of invalid Voice Channels."""


class YTDLSource(discord.PCMVolumeTransformer):

    def __init__(self, source, *, data, requester, path):
        super().__init__(source)
        self.requester = requester
        self.video_url = f"https://www.youtube.com/watch?v={data.get('id')}"
        self.title = data.get('title')
        self.thumbnail = data.get('thumbnail')
        self.uploader_url = data.get('uploader_url')
        self.uploader = data.get('uploader')
        self.channel_url = data.get('channel_url')
        self.web_url = data.get('webpage_url')
        self.path = path

        # YTDL info dicts (data) have other useful information you might want
        # https://github.com/rg3/youtube-dl/blob/master/README.md

    def __getitem__(self, item: str):
        """Allows us to access attributes similar to a dict.

        This is only useful when you are NOT downloading.
        """
        return self.__getattribute__(item)

    @classmethod
    async def create_source(cls, ctx, search: str, *, loop):
        loop = loop or asyncio.get_event_loop()

        to_run = partial(ytdl.extract_info, url=search, download=True)
        data = await loop.run_in_executor(None, to_run)

        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]

        embed = discord.Embed(description=f'Added {data["title"]} to the Queue.', colour=ctx.colour)
        embed.set_thumbnail(url=data["thumbnail"])
        await ctx.send(embed=embed)

        path = ytdl.prepare_filename(data)
        source = cls(discord.FFmpegPCMAudio(path, options=ffmpegopts), data=data, requester=ctx.author, path=path)

        return source


class MusicPlayer:
    """A class which is assigned to each guild using the bot for Music.

    This class implements a queue and loop, which allows for different guilds to listen to different playlists
    simultaneously.

    When the bot disconnects from the Voice it's instance will be destroyed.
    """

    def __init__(self, ctx):
        self.bot = ctx.bot
        self._guild = ctx.guild
        self._channel = ctx.channel
        self._cog = ctx.cog
        self._colour = ctx.colour

        self.files = []
        self.queue = asyncio.Queue()
        self.next = asyncio.Event()

        self.volume = .5
        self.current = None

        ctx.bot.loop.create_task(self.player_loop())

    async def player_loop(self):
        """Our main player loop."""
        await self.bot.wait_until_ready()

        while not self.bot.is_closed():
            self.next.clear()

            try:
                # Wait for the next song. If we timeout cancel the player and disconnect...
                async with timeout(300):  # 5 minutes...
                    source = await self.queue.get()
            except asyncio.TimeoutError:
                return self.destroy(self._guild)

            source.volume = self.volume
            self.current = source

            self._guild.voice_client.play(source, after=lambda _: self.bot.loop.call_soon_threadsafe(self.next.set))
            output = f"Now Playing: {source.title}\nRequested By: {source.requester}"
            embed = discord.Embed(description=output, colour=self._colour)
            embed.set_thumbnail(url=source.thumbnail)
            await self._channel.send(embed=embed)
            await self.next.wait()

            # Make sure the FFPEG process is cleaned up.
            source.cleanup()
            self.current = None

    def destroy(self, guild):
        """Disconnect and cleanup the player."""
        return self.bot.loop.create_task(self._cog.cleanup(guild))


class Music(objects.Cog):
    """Music related commands."""

    def __init__(self, bot):
        self.bot = bot
        self.players = {}

    async def cleanup(self, guild):
        if guild.id in self.players.keys():
            del self.players[guild.id]

        if guild.voice_client:
            await guild.voice_client.disconnect()

    async def cog_check(self, ctx):
        """A local check which applies to all commands in this cog."""
        if not ctx.guild:
            raise commands.NoPrivateMessage
        return True

    async def cog_command_error(self, ctx, error):
        """A local error handler for all errors arising from commands in this cog."""
        if isinstance(error, InvalidVoiceChannel):
            embed = discord.Embed(description=error, colour=ctx.colour)
            await ctx.send(embed=embed)

        elif isinstance(error, VoiceConnectionError):
            embed = discord.Embed(description=error, colour=ctx.colour)
            await ctx.send(embed=embed)

    def get_player(self, ctx):
        """Retrieve the guild player, or generate one."""
        try:
            player = self.players[ctx.guild.id]
        except KeyError:
            player = MusicPlayer(ctx)
            self.players[ctx.guild.id] = player

        return player

    @commands.command(name="connect", brief="Summons the bot to a given voice channel", ignore_extra=False,
                      aliases=['join'])
    async def _connect(self, ctx, *, channel: discord.VoiceChannel = None):
        """Connect to voice. This command also handles moving the bot to different channels.
        ```
        Parameters
        ------------
        channel: [Optional]
            The channel to connect to. 
            If a channel is not specified, an
            attempt to join the voice channel 
            you are in will be made.
        ```
        """
        vc = ctx.voice_client
        if not channel and ctx.author.voice:
            channel = ctx.author.voice.channel

        try:
            if channel:
                if vc:
                    if vc.channel.id == channel.id:
                        output = "Requiem is already in that voice channel!"
                    else:
                        output = f"Alright! Requiem has moved to **{channel.name}**"
                        await vc.move_to(channel)
                else:
                    await channel.connect()
                    output = f"Requiem has joined the channel **{channel.name}**!"
            else:
                output = 'No channel to join. Please either specify a valid channel or join one.'
        except asyncio.TimeoutError:
            output = f'Connecting to channel: **{channel.name}** timed out.'

        embed = discord.Embed(description=output, colour=ctx.colour)
        await ctx.send(embed=embed)

    @commands.command(name="play", brief="Plays a specified song", ignore_extra=False)
    async def _play(self, ctx, *, search: str):
        """Request a song and add it to the queue.
        ```
        This command attempts to join a valid voice channel if the bot is not
        already in one. Uses YTDL to automatically search for and retrieve a song.

        Parameters
        ------------
        search: <Required>
            The song to search and retrieve using YTDL.
            This could be a simple search, an ID or URL.
        ```
        """
        try:
            async with ctx.typing():

                vc = ctx.voice_client

                await ctx.invoke(self._connect) if not vc else vc

                vc = ctx.voice_client
                if vc:
                    player = self.get_player(ctx)

                    source = await YTDLSource.create_source(ctx, search, loop=self.bot.loop)
                    player.files.append(source.path)
                    await player.queue.put(source)

        except youtube_dl.utils.DownloadError:
            embed = discord.Embed(description="There was an issue downloading that track for playback! "
                                              "Please wait a few minutes and try again!", colour=ctx.colour)
            await ctx.send(embed=embed)

    @commands.command(name="pause", brief="Pause the currently playing song", ignore_extra=False)
    async def pause_(self, ctx):
        """Pause the currently playing song."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            output = 'I am not currently connected to a voice channel!'
        elif not vc.is_playing:
            output = "I am not currently playing anything!"
        elif vc.is_paused():
            output = "The song is already paused!"
        else:
            vc.pause()
            output = f'**{ctx.author}**: Paused the song!'

        embed = discord.Embed(description=output, colour=ctx.colour)
        await ctx.send(embed=embed)

    @commands.command(name="resume", brief="Resumes the currently paused song", ignore_extra=False)
    async def resume_(self, ctx):
        """Resume the currently paused song."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            output = 'I am not currently connected to a voice channel!'
        elif not vc.is_paused():
            output = "The song is not currently paused!"
        else:
            output = f'**{ctx.author}**: Resumed the song!'
            vc.resume()

        embed = discord.Embed(description=output, colour=ctx.colour)
        await ctx.send(embed=embed)

    @commands.has_permissions(manage_guild=True)
    @commands.command(name="skip", brief="Skips the current song", ignore_extra=False)
    async def skip_(self, ctx):
        """Skip the song."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            output = 'I am not currently connected to a voice channel!'
        elif not vc.is_playing:
            output = "I am not currently playing anything!"
        else:
            output = f'**{ctx.author}**: Skipped the song!'
            vc.stop()

        embed = discord.Embed(description=output, colour=ctx.colour)
        await ctx.send(embed=embed)

    @commands.command(name="queue", brief="Retrieve a basic queue of upcoming songs", ignore_extra=False,
                      aliases=["lq", "q"])
    async def queue_info(self, ctx):
        """Retrieve a basic queue of upcoming songs."""
        vc = ctx.voice_client
        title = None
        player = self.get_player(ctx)

        if not vc or not vc.is_connected():
            output = 'I am not currently connected to a voice channel!'

        elif player.queue.empty():
            output = 'There are currently no more queued songs.'

        else:
            upcoming = list(player.queue._queue)
            pages = []
            c = 0
            a = 1

            embed = discord.Embed(title=f"Queue - {len(upcoming)}", colour=ctx.colour)
            for source in upcoming:
                if c < 25:
                    c += 1
                    embed.add_field(name=f"{source['title']}", value=f"Requested By {source['requester']}")

                else:
                    pages.append(embed)
                    a += 25
                    c = 1
                    embed = discord.Embed(title=f"Queue - {len(upcoming)}",
                                          colour=ctx.colour)
                    embed.add_field(name=f"{source['title']}", value=f"Requested By {source['requester']}")

            if embed not in pages:
                pages.append(embed)
            paginator = reaction_factory.Paginator(ctx.bot, ctx, ctx.author, pages)
            return await paginator.navigate()

        embed = discord.Embed(title=title, description=output, colour=ctx.colour)
        await ctx.send(embed=embed)

    @commands.command(name="playing", brief="Display information about the currently playing song", ignore_extra=False)
    async def now_playing_(self, ctx):
        """Display information about the currently playing song."""
        vc = ctx.voice_client
        player = self.get_player(ctx)

        if not vc or not vc.is_connected():
            output = 'I am not currently connected to a voice channel!'

        elif not player.current:
            output = "I am not currently playing anything!"

        else:
            output = f"Now Playing: {vc.source.title}\nRequested By: {vc.source.requester}"

        embed = discord.Embed(description=output, colour=ctx.colour)
        embed.set_thumbnail(url=vc.source.thumbnail)
        await ctx.send(embed=embed)

    @commands.has_permissions(manage_guild=True)
    @commands.command(name="volume", brief="Change the player volume", ignore_extra=False)
    async def change_volume(self, ctx, *, volume: float):
        """Change the player volume.
        ```
        Parameters
        ------------
        volume: <Required>
            The volume to set the player to in
            percentage. This must be between 1 and 100.
        ```
        """
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            output = 'I am not currently connected to a voice channel!'

        elif not 0 < volume < 101:
            output = 'Please enter a value between 1 and 100.'

        else:

            player = self.get_player(ctx)

            if vc.source:
                vc.source.volume = volume / 100

            player.volume = volume / 100
            output = f'**{ctx.author}** set the volume to **{volume}%**'

        embed = discord.Embed(description=output, colour=ctx.colour)
        await ctx.send(embed=embed)

    @commands.has_permissions(manage_guild=True)
    @commands.command(name="stop", brief="Stop the currently playing song and destroy the player", ignore_extra=False)
    async def stop_(self, ctx):
        """Stop the currently playing song and destroy the player.
        ```
        !Warning!
            This will destroy the player assigned to your guild,
            also deleting any queued songs and settings.
        ```
        """
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            output = 'I am not currently connected to a voice channel!'

        else:
            output = "The player has been stopped!"
            await self.cleanup(ctx.guild)

        embed = discord.Embed(description=output, colour=ctx.colour)
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Music(bot))
