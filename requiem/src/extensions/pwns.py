# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import objects
from pwpy import EventClient, ScrapeClient
from src.functions import pwns_war_monitor, pwns_applicant_monitor, pwns_beige_monitor


class PWNS(objects.Cog):
    """! Politics and War Notification Service"""

    def __init__(self, bot):
        self.bot = bot
        self.name = "PWNS"
        self.bot.pw = EventClient(
            bot.config.pnw_token, timeout=300, verify=True)
        self.bot.pwscrape = ScrapeClient(
            bot.config.pnw_email, bot.config.pnw_pass)
        self.bot.pw.run()
        self.bot.pw.add_listener(self.war_monitor, "on_war_declare")
        self.bot.pw.add_listener(
            self.applicant_monitor, "on_nation_applied_alliance")
        self.bot.pw.add_listener(self.beige_monitor, "on_nation_exited_beige")

    async def war_monitor(self, war, war_last):
        await pwns_war_monitor.run_war_monitor(self, war)

    async def applicant_monitor(self, nation, nation_last):
        await pwns_applicant_monitor.run_applicant_monitor(self, nation)

    async def beige_monitor(self, nation, nation_after):
        await pwns_beige_monitor.run_beige_monitor(self, nation)


def setup(bot):
    bot.add_cog(PWNS(bot))
